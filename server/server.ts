import {Database} from './database/database';
import {Webserver} from './webserver';
import {WebsocketServer} from './websocketServer';

class Server {
  private _database: Database;
  private _webserver: Webserver;
  private _websocketServer: WebsocketServer;

  public constructor() {
    this._database = new Database();
    this._webserver = new Webserver();
    this._websocketServer = new WebsocketServer();
  }

  public getDatabase() {
    return this._database;
  }

  public getWebserver() {
    return this._webserver;
  }

  public getWebsocketServer() {
    return this._websocketServer;
  }

}

// Kick everything off!
export var AppServer = new Server();
