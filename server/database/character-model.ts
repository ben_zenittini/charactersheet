
export class CharacterModel {
  private _model: any;

  public constructor(mongoose: any) {
    var schema = mongoose.Schema({
      name: String,
      notes: String,
      actions: [{
        name: String,
        category: String,
        notes: String,
        subactions: [{
          uuid: String,
          is_included: Boolean,
          value: String,
          name: String,
          notes: String
        }]
      }],
      playerStats: [{
        name: String,
        category: String,
        notes: String,
        properties: [{
          uuid: String,
          is_visible: Boolean,
          property: String,
          value: String
        }]
      }],
    });
    this._model = mongoose.model('characters', schema);
  }

  public saveCharacter(inChar: any) {
    this._model.findById(inChar._id).exec(function(err, character) {
      if (err) throw err;
      character.name = inChar.name;
      character.notes = inChar.notes;
      character.actions = inChar.actions;
      character.playerStats = inChar.playerStats;
      character.save(function(err){if (err) throw err;});
    });
  }

  public createCharacter(name: String, callback?: any){
    var newCharacter = new this._model({
        "name":name,
        "notes":"Put campaign notes here.",

        "playerStats":[

        { "name":"Health", "category":"Player", "notes":"", "properties":[ { "uuid":"253932547544", "is_visible":true, "property":"Current", "value":"20", }, { "uuid":"795874772771", "is_visible":true, "property":"Max", "value":"20", } ] },
        { "name":"Speed", "category":"Player", "notes":"", "properties":[ { "uuid":"991740766512", "is_visible":true, "property":"Base", "value":"30 ft", }, { "uuid":"552893177238", "is_visible":false, "property":"Swim", "value":"30 ft", }, { "uuid":"685878202240", "is_visible":false, "property":"Climb", "value":"30 ft", } ] },
        { "name":"Languages", "category":"Player", "notes":"", "properties":[ { "uuid":"373725122536", "is_visible":true, "property":"Common", "value":"", } ] },
        { "name":"Class (______)", "category":"Player", "notes":"", "properties":[ { "uuid":"56121125452", "is_visible":true, "property":"Level", "value":"1", }, { "uuid":"730754893386", "is_visible":false, "property":"Skill Ranks / Level", "value":"6 + Int Mod", }, { "uuid":"502166338383", "is_visible":false, "property":"Hit Die", "value":"1d6", }, { "uuid":"104186663312", "is_visible":false, "property":"Fortitude", "value":"0", }, { "uuid":"99834395637", "is_visible":false, "property":"Reflex", "value":"0", }, { "uuid":"416543737750", "is_visible":false, "property":"Will", "value":"0", }, { "uuid":"758252405636", "is_visible":false, "property":"Base Attack Bonus", "value":"0", } ] },

        { "name":"Money", "category":"Inventory", "notes":"", "properties":[ { "uuid":"532696514550", "is_visible":true, "property":"Gold", "value":"0", }, { "uuid":"456821885336", "is_visible":true, "property":"Silver", "value":"0", }, { "uuid":"705065753263", "is_visible":true, "property":"Copper", "value":"0", } ] },

        { "name":"Strength", "category":"Ability Scores", "notes":"", "properties":[ { "uuid":"775459600177", "is_visible":true, "property":"Score", "value":"10", }, { "uuid":"66955329759", "is_visible":true, "property":"Modifier", "value":"0", } ] },
        { "name":"Dexterity", "category":"Ability Scores", "notes":"", "properties":[ { "uuid":"983612251463", "is_visible":true, "property":"Score", "value":"10", }, { "uuid":"356651431477", "is_visible":true, "property":"Modifier", "value":"0", } ] },
        { "name":"Constitution", "category":"Ability Scores", "notes":"", "properties":[ { "uuid":"111897366157", "is_visible":true, "property":"Score", "value":"10", }, { "uuid":"739661461278", "is_visible":true, "property":"Modifier", "value":"0", } ] },
        { "name":"Intelligence", "category":"Ability Scores", "notes":"", "properties":[ { "uuid":"290962798475", "is_visible":true, "property":"Score", "value":"10", }, { "uuid":"33386352198", "is_visible":true, "property":"Modifier", "value":"0", } ] },
        { "name":"Wisdom", "category":"Ability Scores", "notes":"", "properties":[ { "uuid":"113171537120", "is_visible":true, "property":"Score", "value":"10", }, { "uuid":"635037986124", "is_visible":true, "property":"Modifier", "value":"0", } ] },
        { "name":"Charisma", "category":"Ability Scores", "notes":"", "properties":[ { "uuid":"951899889017", "is_visible":true, "property":"Score", "value":"10", }, { "uuid":"380276625616", "is_visible":true, "property":"Modifier", "value":"0", } ] }
        ],

          "actions":[

          { "name":"Fish Slap (att)", "category":"Attacks", "notes":"Crit: 18-20 / x5", "subactions":[ { "uuid":"117555059983", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"490282671959", "is_included":true, "value":"${758252405636}", "name":"BAB", "notes":"", }, { "uuid":"822721890213", "is_included":true, "value":"${66955329759}", "name":"Str Mod", "notes":"", } ] },
          { "name":"Fish Slap (dmg)", "category":"Attacks", "notes":"Type: Bludgeoning", "subactions":[ { "uuid":"679596689721", "is_included":true, "value":"2d8", "name":"Dice Roll", "notes":"", } ] },
          { "name":"CMB", "category":"Attacks", "notes":"", "subactions":[ { "uuid":"164635330034", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"880615324225", "is_included":true, "value":"${758252405636}", "name":"BAB", "notes":"", }, { "uuid":"883846170903", "is_included":true, "value":"${66955329759}", "name":"Str Mod", "notes":"", } ] },

          { "name":"Fortitude", "category":"Defenses", "notes":"", "subactions":[ { "uuid":"792005869609", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"823297205859", "is_included":true, "value":"${104186663312}", "name":"Base", "notes":"", }, { "uuid":"530597928205", "is_included":true, "value":"${739661461278}", "name":"Con Mod", "notes":"", } ] },
          { "name":"Reflex", "category":"Defenses", "notes":"", "subactions":[ { "uuid":"535483376801", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"102653561968", "is_included":true, "value":"${99834395637}", "name":"Base", "notes":"", }, { "uuid":"276347130763", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", } ] },
          { "name":"Will", "category":"Defenses", "notes":"", "subactions":[ { "uuid":"979403352833", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"512200801381", "is_included":true, "value":"${416543737750}", "name":"Base", "notes":"", }, { "uuid":"18858403368", "is_included":true, "value":"${635037986124}", "name":"Wis Mod", "notes":"", } ] },
          { "name":"Armor Class (AC)", "category":"Defenses", "notes":"", "subactions":[ { "uuid":"323556628832", "is_included":true, "value":"10", "name":"Base", "notes":"", }, { "uuid":"589052812091", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"927241534042", "is_included":true, "value":"0", "name":"Armor", "notes":"", } ] },
          { "name":"Touch AC", "category":"Defenses", "notes":"", "subactions":[ { "uuid":"501552778171", "is_included":true, "value":"10", "name":"Base", "notes":"", }, { "uuid":"475702926844", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", } ] },
          { "name":"Flat-Footed AC", "category":"Defenses", "notes":"", "subactions":[ { "uuid":"477794663743", "is_included":true, "value":"10", "name":"Base", "notes":"", }, { "uuid":"5220030016", "is_included":true, "value":"0", "name":"Armor", "notes":"", } ] },
          { "name":"CMD", "category":"Defenses", "notes":"", "subactions":[ { "uuid":"76850169592", "is_included":true, "value":"${758252405636}", "name":"BAB", "notes":"", }, { "uuid":"959346836253", "is_included":true, "value":"${66955329759}", "name":"Str Mod", "notes":"", }, { "uuid":"714257984354", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"150186060585", "is_included":true, "value":"10", "name":"Base", "notes":"", } ] },

          { "name":"Acrobatics", "category":"Skills", "notes":"", "subactions":[ { "uuid":"537111017171", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"291133671996", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"522618649039", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Appraise", "category":"Skills", "notes":"", "subactions":[ { "uuid":"469748230270", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"182100846733", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"645130919430", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Bluff", "category":"Skills", "notes":"", "subactions":[ { "uuid":"313981605084", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"271352094902", "is_included":true, "value":"${380276625616}", "name":"Cha Mod", "notes":"", }, { "uuid":"471891709791", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Climb", "category":"Skills", "notes":"", "subactions":[ { "uuid":"493430991211", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"815900433080", "is_included":true, "value":"${66955329759}", "name":"Str Mod", "notes":"", }, { "uuid":"267426030758", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Diplomacy", "category":"Skills", "notes":"", "subactions":[ { "uuid":"254493169158", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"739845989474", "is_included":true, "value":"${380276625616}", "name":"Cha Mod", "notes":"", }, { "uuid":"499916966443", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Disable Device", "category":"Skills", "notes":"", "subactions":[ { "uuid":"553031527493", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"154904289173", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"875307202931", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Disguise", "category":"Skills", "notes":"", "subactions":[ { "uuid":"622867227382", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"262329814234", "is_included":true, "value":"${380276625616}", "name":"Cha Mod", "notes":"", }, { "uuid":"845114326647", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Escape Artist", "category":"Skills", "notes":"", "subactions":[ { "uuid":"107343675347", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"262001456876", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"785968110129", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Fly", "category":"Skills", "notes":"", "subactions":[ { "uuid":"344655299439", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"410587969144", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"118364391024", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Handle Animal", "category":"Skills", "notes":"", "subactions":[ { "uuid":"535764483716", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"694789326778", "is_included":true, "value":"${380276625616}", "name":"Cha Mod", "notes":"", }, { "uuid":"360863834219", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Heal", "category":"Skills", "notes":"", "subactions":[ { "uuid":"315835653821", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"375465689943", "is_included":true, "value":"${635037986124}", "name":"Wis Mod", "notes":"", }, { "uuid":"240277667974", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Intimidate", "category":"Skills", "notes":"", "subactions":[ { "uuid":"255132123926", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"949822166841", "is_included":true, "value":"${380276625616}", "name":"Cha Mod", "notes":"", }, { "uuid":"470259967398", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Arcana)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"310824299661", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"899042094953", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"284218537323", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Dungeoneering)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"94692082276", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"934563649164", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"609495569599", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Engineering)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"750384972749", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"817875037485", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"907928134487", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Geography)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"919206370849", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"122425916940", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"108048017182", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (History)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"952525401284", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"976319765880", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"9209288240", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Local)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"362860273979", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"498190722030", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"374638037081", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Nature)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"836918611016", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"339808752241", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"544519245421", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Nobility)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"478811245928", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"569576679401", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"388187590463", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Planes)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"863039358774", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"801612492429", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"369542546056", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Knowledge (Religion)", "category":"Skills", "notes":"", "subactions":[ { "uuid":"195112449609", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"634493190386", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"100336844193", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Linguistics", "category":"Skills", "notes":"", "subactions":[ { "uuid":"517441400046", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"795480552519", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"223930278923", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Perception", "category":"Skills", "notes":"", "subactions":[ { "uuid":"499175243753", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"867442835510", "is_included":true, "value":"${635037986124}", "name":"Wis Mod", "notes":"", }, { "uuid":"160042646932", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Ride", "category":"Skills", "notes":"", "subactions":[ { "uuid":"680733788055", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"367383685442", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"492286909248", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Sense Motive", "category":"Skills", "notes":"", "subactions":[ { "uuid":"948040981633", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"202723708741", "is_included":true, "value":"${635037986124}", "name":"Wis Mod", "notes":"", }, { "uuid":"529476197170", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Sleight of Hand", "category":"Skills", "notes":"", "subactions":[ { "uuid":"119346853250", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"505324321478", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"177033478323", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Spellcraft", "category":"Skills", "notes":"", "subactions":[ { "uuid":"870418314740", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"313590132418", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", }, { "uuid":"544332218265", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Stealth", "category":"Skills", "notes":"", "subactions":[ { "uuid":"921165092825", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"859479765888", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", }, { "uuid":"450466045392", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Survival", "category":"Skills", "notes":"", "subactions":[ { "uuid":"651145191449", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"795304402017", "is_included":true, "value":"${635037986124}", "name":"Wis Mod", "notes":"", }, { "uuid":"976380151338", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Swim", "category":"Skills", "notes":"", "subactions":[ { "uuid":"739723457076", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"298076245032", "is_included":true, "value":"${66955329759}", "name":"Str Mod", "notes":"", }, { "uuid":"984093400176", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },
          { "name":"Use Magic Device", "category":"Skills", "notes":"", "subactions":[ { "uuid":"478069022152", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"322470188673", "is_included":true, "value":"${380276625616}", "name":"Cha Mod", "notes":"", }, { "uuid":"222337886283", "is_included":true, "value":"0", "name":"Ranks", "notes":"", } ] },

          { "name":"Strength", "category":"Core Stats", "notes":"", "subactions":[ { "uuid":"566267670619", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"789183201014", "is_included":true, "value":"${66955329759}", "name":"Str Mod", "notes":"", } ] },
          { "name":"Dexterity", "category":"Core Stats", "notes":"", "subactions":[ { "uuid":"410155726068", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"433433227918", "is_included":true, "value":"${356651431477}", "name":"Dex Mod", "notes":"", } ] },
          { "name":"Constitution", "category":"Core Stats", "notes":"", "subactions":[ { "uuid":"584138886598", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"261710196215", "is_included":true, "value":"${739661461278}", "name":"Con Mod", "notes":"", } ] },
          { "name":"Intelligence", "category":"Core Stats", "notes":"", "subactions":[ { "uuid":"60737055843", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"989566521242", "is_included":true, "value":"${33386352198}", "name":"Int Mod", "notes":"", } ] },
          { "name":"Wisdom", "category":"Core Stats", "notes":"", "subactions":[ { "uuid":"956628195282", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"624812586926", "is_included":true, "value":"${635037986124}", "name":"Wis Mod", "notes":"", } ] },
          { "name":"Charisma", "category":"Core Stats", "notes":"", "subactions":[ { "uuid":"949846706598", "is_included":true, "value":"1d20", "name":"Dice Roll", "notes":"", }, { "uuid":"481029744990", "is_included":true, "value":"${380276625616}", "name":"Cha Mod", "notes":"", } ] }
    ]
    });

    newCharacter.save(function(err) {
      if (err) throw err;
      if (callback) callback(newCharacter);
    });
  }
}
