declare function require(name:string);

import {CampaignModel} from './campaign-model';
import {CharacterModel} from './character-model';

export class Database {
  private _mongoose: any;

  private _campaignModel: CampaignModel;
  private _characterModel: CharacterModel;

  public constructor() {
    this._mongoose = require('mongoose');
    this._mongoose.connect('mongodb://localhost/charsheetv1');

    this._campaignModel = new CampaignModel(this._mongoose);
    this._characterModel = new CharacterModel(this._mongoose);
  }

  public getCampaignModel() {
    return this._campaignModel;
  }

  public getCharacterModel() {
    return this._characterModel;
  }
}

