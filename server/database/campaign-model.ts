
export class CampaignModel {
  private _model: any;

  public constructor(mongoose: any) {
    var schema = mongoose.Schema({
      name: String,
      password: String,
      characters: [{ type: mongoose.Schema.Types.ObjectId, ref: 'characters' }]
    });
    this._model = mongoose.model('campaigns', schema);
  }

  public getAllCampaignNames(callback?: any) {
    this._model.find().exec(function(err, campaigns) {
      if (err) throw err;
      if (callback) callback(campaigns);
    });
  }

  public getCampaignData(campaignId: String, callback?: any) {
    this._model.findById(campaignId).populate('characters').exec(function(err, campaigns) {
      if (err) throw err;
      if (callback) callback(campaigns);
    });
  }

  public createCampaign(name: String, password: String, callback?: any) {
    var newCampaign = new this._model({
      name: name,
      password: password,
      characters: []
    });

    newCampaign.save(function(err) {
      if (err) throw err;
      if (callback) callback(newCampaign);
    });
  }

  public addCharacter(campaignId: String, characterId: String, callback?: any) {
    this._model.findById(campaignId).exec(function(err, campaign) {
      if (err) throw err;
      campaign.characters.push(characterId);
      campaign.save(function(err) {
        if (err) throw err;
        if (callback) callback(campaign);
      });
    });
  }

  public removeCharacter(campaignId: String, characterId: String) {
  }

  public deleteCampaign(campaignId: String) {
  }
}

