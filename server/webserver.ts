declare function require(name:string);
declare var __dirname:string;

export class Webserver {
  private _express: any;
  private _path: any;

  public constructor() {
    this._express = require('express');
    this._path = require('path');

    this._express.Router().get('/', function(req, res) {
        res.sendFile(__dirname + '/public/index.html');
    });

    var app = this._express();

    // Tell express everything in the client is open to serve.
    app.use(this._express.static(this._path.join(__dirname, "../client")));

    // Let 'er rip!
    var server = app.listen(3000, function() {
        var host = server.address().address;
        var port = server.address().port;

        console.log('OurPG listening at http://%s:%s', host, port);
    });
  }

}
