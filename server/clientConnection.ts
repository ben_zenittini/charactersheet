import {AppServer} from './server';

export class ClientConnection {
  private _conn: any;

  public constructor(conn: any) {
    this._conn = conn;

    // A convenience function
    conn.sendObject = function(obj) {
      conn.sendText(JSON.stringify(obj));
    };

    conn.on("text", this.onText);
    conn.on("close", this.onClose);
    conn.on("error", function(err) {
      // This handler is required or nodemon will crash when a user closes their browser.
    });
  }

  public sendObject(obj) {
    this._conn.sendObject(obj);
  }

  public onClose(code: string, reason: string) {
    console.log("A client disconnected.");
    // TODO - remove them from the list of connected clients..?
  }

  public onText(inReq: string) {
    var data = JSON.parse(inReq);

    if (data.type === 'DiceRoll') {
      // Relay the dice roll action to all other clients
      AppServer.getWebsocketServer().broadcastAll(data);
    } else if (data.type === 'CharacterUpdate') {
      AppServer.getDatabase().getCharacterModel().saveCharacter(data.character);
    } else if (data.type === 'PlayerRequest') {
      AppServer.getDatabase().getCampaignModel().getCampaignData(data.campaignId, (camp) => {
        this.sendObject({
          'type': 'CharacterData',
          'characterData': camp
        });
      });
    } else if (data.type === 'CreateCampaign') {
      console.log("Creating a campaign: " + data.campaignName);
      AppServer.getDatabase().getCampaignModel().createCampaign(data.campaignName, data.campaignPassword);
      // TODO - add a callback that refreshes the client's campaign list.
    } else if (data.type === 'CreateCharacter') {
      AppServer.getDatabase().getCharacterModel().createCharacter(data.characterName, (character) => {
        AppServer.getDatabase().getCampaignModel().addCharacter(data.activeCampaign, character._id, (camp) => {
          AppServer.getDatabase().getCampaignModel().getCampaignData(data.activeCampaign, (populatedCamp) => {
            this.sendObject({
              'type': 'CharacterData',
              'characterData': populatedCamp
            });
          });
        });
      });
    } else {
      console.log("Unrecognized data type: " + data.type);
    }
  }

}
