import {ClientConnection} from './clientConnection';
import {AppServer} from './server';

declare function require(name:string);

export class WebsocketServer {
  private _connectedClients: ClientConnection[];

  public constructor() {
    this._connectedClients = [];
    var ws = require("nodejs-websocket");
    ws.createServer((conn) => this._newClient(conn)).listen(8001);
  }

  private _newClient(conn: any) {
    var client = new ClientConnection(conn);
    this._connectedClients.push(client);

    // Send list of thingies to the new client
    var db = AppServer.getDatabase();
    db.getCampaignModel().getAllCampaignNames((camps) => {
      client.sendObject({
        'type': 'CampaignList',
        'campaignList': camps
      });
    });
  }

  public broadcastAll(message: any) {
    this._connectedClients.forEach((client) => {
      client.sendObject(message);
    });
  }

}
