import {Injectable} from 'angular2/core';

@Injectable()
export class FilterService {
  public filter: string;

  getFilterText() {
    if (this.filter == undefined) return "";
    return this.filter;
  }

  setFilterText(text) {
    this.filter = text;
  }
}
