import {Injectable} from 'angular2/core';

import {PlayerStatService} from '../player-stat/player-stat.service';
import {ActionService} from '../action/action.service';

import {Campaign} from '../../interfaces/campaign';
import {Player} from '../../interfaces/player';
import {PlayerStat} from '../../interfaces/player-stat';
import {SubPlayerStat} from '../../interfaces/sub-player-stat';
import {Action} from '../../interfaces/action';
import {Subaction} from '../../interfaces/subaction';

@Injectable()
export class CampaignService {

  private campaigns: Campaign[];
  private activeCampaign: Campaign;

  public constructor(
    private _playerStatService: PlayerStatService,
    private _actionService: ActionService) {
    this.setActiveCampaign(null);
    this.setCampaigns([]);
  }

  public setCampaigns(campaigns: Campaign[]) {
    this.campaigns = campaigns;
  }

  public getCampaigns() {
    return this.campaigns;
  }

  public resetCharacterSheet() {
    this._actionService.reset();
    this._playerStatService.reset();
  }

  public setActiveCampaign(campaign: any) {
    if (campaign === null) {
      this.activeCampaign = null;
    } else {
      this.activeCampaign = new Campaign(campaign._id, campaign.name, campaign.password, []);
      campaign.characters.forEach((character) => {
        var player = new Player(character._id, character.name);
        player.setNotes(character.notes);
        player.actions = character.actions;
        player.playerStats = character.playerStats;
        this.activeCampaign.addPlayer(player);
      });
    }
  }

  public setActivePlayer(player: Player) {
    this.resetCharacterSheet();

    this.getActiveCampaign().setActivePlayer(player);

    player.actions.forEach((act) => {
      var action = new Action(act.name, [], act.notes);
      act.subactions.forEach((sub) => {
        action.addSubaction(new Subaction(sub.uuid, sub.name, sub.value, sub.notes, sub.is_included));
      });
      this._actionService.addAction(act.category, action);
    });

    player.playerStats.forEach((ps) => {
      var playerStat = new PlayerStat(ps.name, [], ps.notes);
      ps.properties.forEach((prop) => {
        playerStat.addSubPlayerStat(new SubPlayerStat(prop.uuid, prop.property, prop.value, prop.is_visible));
      });
      this._playerStatService.addPlayerStat(ps.category, playerStat);
    });
  }

  public getActiveCampaign() {
    return this.activeCampaign;
  }
}
