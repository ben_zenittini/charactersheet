import {Injectable} from 'angular2/core';

import {RollResult} from '../../interfaces/rollresult';
import {DiceRoll} from '../../interfaces/diceroll';
import {Action} from '../../interfaces/action';

import {StatlinkService} from '../statlink/statlink.service';

@Injectable()
export class DiceRollService {
  public rolls: DiceRoll[] = [];

  public constructor(private _statlinkService: StatlinkService) {}

  public reset() {
    this.rolls.length = 0;
  }

  public addRoll(newRoll: DiceRoll) {
    this.rolls.unshift(newRoll) > 10 && this.rolls.pop();
  }

  public getRolls() {
    return this.rolls;
  }

  public roll(characterName: String, action: Action) {
    var diceRoll = new DiceRoll(characterName, action.name, action.subactions);

    // Compute roll results
    let results: Array<RollResult> = [], total: number = 0;
    for (let subAction of diceRoll.subActions) {
      if (subAction.getIsIncluded()) {
        let valueString: string = this._statlinkService.resolve(subAction.getValue()).trim(), numericResult: number;
        if (DiceRoll.rollPattern.test(valueString)) {
          numericResult = DiceRoll.rollThisString(valueString);
        } else if (valueString !== "" && !isNaN(+valueString)) {
          numericResult = parseFloat(valueString);
        } else {
          numericResult = 0;
        }
        results.push(new RollResult(subAction.getName().trim(), numericResult, valueString));
        total += numericResult;
      }
    }

    diceRoll.rollResults = results;
    diceRoll.rollTotal = total;

    return diceRoll;
  }
}
