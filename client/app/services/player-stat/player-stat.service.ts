import {Injectable} from 'angular2/core';

import {PlayerStat} from '../../interfaces/player-stat';
import {PlayerStatCategory} from '../../interfaces/player-stat-category';

@Injectable()
export class PlayerStatService {

  private _playerStatCategories: PlayerStatCategory[];

  public constructor() {
    this._playerStatCategories = [];
  }

  public reset() {
    this._playerStatCategories.length = 0;
  }

  public getPlayerStatCategories() {
    return this._playerStatCategories;
  }

  public deletePlayerStat(category: String, playerStat: PlayerStat) {
    this._playerStatCategories.forEach((psc) => {
      if (psc.getName() === category) {
        psc.deletePlayerStat(playerStat);
      }
    });
  }

  public addCategory(category: String) {
    this._playerStatCategories.push(new PlayerStatCategory(category));
  }

  public addPlayerStat(category: String, playerStat: PlayerStat) {
    var added = false;
    this._playerStatCategories.forEach((psc) => {
      if (psc.getName() === category) {
        psc.addPlayerStat(playerStat);
        added = true;
      }
    });

    // If the category didn't exist, then create it.
    if (!added) {
      var psc = new PlayerStatCategory(category);
      psc.addPlayerStat(playerStat);
      this._playerStatCategories.push(psc);
    }
  }

}

