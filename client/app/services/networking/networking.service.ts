import {Injectable} from 'angular2/core';

@Injectable()
export class NetworkingService {
  private _ws: WebSocket;

  public constructor() {
    this._ws = new WebSocket('ws://localhost:8001');
    //this._ws = new WebSocket('ws://69.249.253.239:8001');
    //this._ws = new WebSocket('ws://ourpg.me:8001');

    this._ws.onopen = function(event) { console.log("Good day, Etho!"); };
    this._ws.onerror = function(event) {
      console.log(event);
    };
    this._ws.onclose = function(event) {
      console.log(event);
      console.log("Take care. Haaave a good day. Bye bye.");
      alert("Server disconnected. Try reloading your page. (She's dead, Jim.)");
    };
  }

  public sendObject(obj: any) {
    this._ws.send(JSON.stringify(obj));
  }

  public setOnMessage(onMessageFn: any) {
    this._ws.onmessage = (event) => {onMessageFn(event)};
  }

}
