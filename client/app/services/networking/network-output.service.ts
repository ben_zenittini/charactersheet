import {Injectable} from 'angular2/core';

import {NetworkingService} from './networking.service';
import {CampaignService} from '../campaign/campaign.service';

@Injectable()
export class NetworkOutputService {

  public constructor(private _networkingService: NetworkingService, private _campaignService: CampaignService) {}

  public sendObject(obj: any) {
    this._networkingService.sendObject(obj);
  }

  // -- Utility functions for generating message objects

  public sendDiceRoll(diceRoll: any) {
    this.sendObject({
      'type': 'DiceRoll',
      'diceRoll': diceRoll
    });
  }

  public sendPlayerRequest(campaignId: any) {
    this.sendObject({
      'type': 'PlayerRequest',
      'campaignId': campaignId
    });
  }

  public sendCreateCharacter(characterName: String) {
    this.sendObject({
      'type': 'CreateCharacter',
      'activeCampaign': this._campaignService.getActiveCampaign().getId(),
      'characterName': characterName
    });
  }

  public sendCreateCampaign(campaignName: String, campaignPassword: String) {
    this.sendObject({
      'type': 'CreateCampaign',
      'campaignName': campaignName,
      'campaignPassword': campaignPassword
    });
  }

  public sendCharacterUpdate(character: any) {
    this.sendObject({
      'type': 'CharacterUpdate',
      'character': character
    });
  }

}
