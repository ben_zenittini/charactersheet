import {Injectable} from 'angular2/core';

import {Campaign} from '../../interfaces/campaign';

import {NetworkingService} from './networking.service';
import {DiceRollService} from '../dice-roll/dice-roll.service';
import {CampaignService} from '../campaign/campaign.service';

@Injectable()
export class NetworkInputService {

  public constructor(private _diceRollService: DiceRollService, private _campaignService: CampaignService, private _networkingService: NetworkingService) {
    this._networkingService.setOnMessage((event) => {this.onMessageFn(event)});
  }

  private onMessageFn(event) {
    var data = JSON.parse(event.data);

    if (data.type === 'DiceRoll') {
      this._diceRollService.addRoll(data.diceRoll);
    } else if (data.type === 'CampaignList') {
      var campList = [];
      data.campaignList.forEach((camp) => {
        campList.push(new Campaign(camp._id, camp.name, camp.password, []));
      });
      this._campaignService.setCampaigns(campList);
    } else if (data.type === 'CharacterData') {
      this._campaignService.setActiveCampaign(data.characterData);
    }
  }


}
