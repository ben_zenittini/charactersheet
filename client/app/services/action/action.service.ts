import {Injectable} from 'angular2/core';

import {Action} from '../../interfaces/action';
import {ActionCategory} from '../../interfaces/action-category';

@Injectable()
export class ActionService {

  private _actionCategories: ActionCategory[];

  public constructor() {
    this._actionCategories = [];
  }

  public reset() {
    this._actionCategories.length = 0;
  }

  public getActionCategories() {
    return this._actionCategories;
  }

  public deleteAction(category: String, action: Action) {
    this._actionCategories.forEach((ac) => {
      if (ac.getName() === category) {
        ac.deleteAction(action);
      }
    });
  }

  public addCategory(category: String) {
    this._actionCategories.push(new ActionCategory(category));
  }

  public addAction(category: String, action: Action) {
    var added = false;
    this._actionCategories.forEach((ac) => {
      if (ac.getName() === category) {
        ac.addAction(action);
        added = true;
      }
    });

    // If the category didn't exist, then create it.
    if (!added) {
      var ac = new ActionCategory(category);
      ac.addAction(action);
      this._actionCategories.push(ac);
    }
  }

}

