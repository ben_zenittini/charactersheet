import {Injectable} from 'angular2/core';

import {NetworkOutputService} from '../networking/network-output.service';
import {CampaignService} from '../campaign/campaign.service';
import {PlayerStatService} from '../player-stat/player-stat.service';
import {ActionService} from '../action/action.service';

@Injectable()
export class PersistenceService {

  private _dirty: boolean = false;
  private _timeout: any;

  public constructor(
    private _campaignService: CampaignService,
    private _networkOutputService: NetworkOutputService,
    private _actionService: ActionService,
    private _playerStatService: PlayerStatService) {}

  public isDirty() {
    return this._dirty;
  }

  public markDirty() {
    // TODO - delete these lines
    console.log("Marked dirty!");

    if (this._campaignService.getActiveCampaign() !== null && this._campaignService.getActiveCampaign().activePlayer !== null) {
      if (this._timeout) {
        clearTimeout(this._timeout);
      }
      this._timeout = setTimeout(() => {
        // Build and send server update
        var activePlayer = this._campaignService.getActiveCampaign().activePlayer;
        var character = {
          '_id': activePlayer.getId(),
          'name': activePlayer.name,
          'notes': activePlayer.notes,
          'actions': [],
          'playerStats': []
        };

        // Actions
        this._actionService.getActionCategories().forEach((cat) => {
          cat.actions.forEach((act) => {
            var a = {
              'name': act.name,
              'category': cat.categoryName,
              'notes': act.notes,
              'subactions': []
            };
            act.subactions.forEach((subact) => {
              a.subactions.push({
                'uuid': subact.uuid,
                'is_included': subact.isIncluded,
                'value': subact.value,
                'name': subact.name,
                'notes': subact.description
              });
            });
            character.actions.push(a);
          });
        });

        // Player Stats
        this._playerStatService.getPlayerStatCategories().forEach((cat) => {
          cat.playerStats.forEach((ps) => {
            var s = {
              'name': ps.name,
              'category': cat.categoryName,
              'notes': ps.notes,
              'properties': []
            };
            ps.subPlayerStats.forEach((prop) => {
              s.properties.push({
                'uuid': prop.uuid,
                'is_visible': prop.showInHeading,
                'property': prop.key,
                'value': prop.value
              });
            });
            character.playerStats.push(s);
          });
        });

        this._networkOutputService.sendCharacterUpdate(character);

        // TODO delete me in favor of an on-screen visualization
        console.log("Saved!");

        // Clear the dirty flag
        this._dirty = false;
      }, 5000);
      this._dirty = true;
    }
  }

}
