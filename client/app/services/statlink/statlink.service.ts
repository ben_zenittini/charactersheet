import {Injectable} from 'angular2/core';

import {Subaction} from '../../interfaces/subaction';

import {PlayerStatService} from '../../services/player-stat/player-stat.service';
import {ActionService} from '../../services/action/action.service';
import {PersistenceService} from '../../services/persistence/persistence.service';

@Injectable()
export class StatlinkService {

  public isLinking: boolean = false;
  public linkBeginning: Subaction = null;

  public constructor(private _persistenceService: PersistenceService, private _playerStatService: PlayerStatService, private _actionService: ActionService) {}

  public beginLink(subaction: Subaction) {
    this.linkBeginning = subaction;
    this.isLinking = true;
  }

  public endLink(target: number) {
    this.linkBeginning.setLinkTarget(target);

    this.isLinking = false;
    this.linkBeginning = null;

    this._persistenceService.markDirty();
  }

  public cancelLink() {
    this.isLinking = false;
    this.linkBeginning = null;
  }

  public resolve(target: String) {
    if (target.toString().startsWith("${") && target.toString().endsWith("}")) {
      var trimmedTarget = target.substring(2, target.length-1);
      var resolvedValue = null;

      // Check PlayerStat subproperties
      this._playerStatService.getPlayerStatCategories().forEach((cat) => { 
        cat.playerStats.forEach((ps) => {
          ps.subPlayerStats.forEach((sps) => {
            if (sps.uuid.toString() === trimmedTarget) {
            resolvedValue = sps.value;
            }
          });
        });
      });
      if (resolvedValue !== null) {
        if (resolvedValue.toString().startsWith("${") && resolvedValue.toString().endsWith("}")) {
          return this.resolve(resolvedValue);
        } else {
          return resolvedValue;
        }
      }

      // Check subactions
      this._actionService.getActionCategories().forEach((cat) => {
        cat.actions.forEach((act) => {
          act.subactions.forEach((subact) => {
            if (subact.uuid.toString() === trimmedTarget) {
              resolvedValue = subact.value;
            }
          });
        });
      });
      if (resolvedValue !== null) {
        if (resolvedValue.toString().startsWith("${") && resolvedValue.toString().endsWith("}")) {
          return this.resolve(resolvedValue);
        } else {
          return resolvedValue;
        }
      }
    }
    return target;
  }

}
