import {DiceRoll} from './diceroll';

export class RollResult {

  public description: string;
  public value: number;
  public maxPossible: number; // Maximum possible value for a dice roll or undefined if not applicable
  public minPossible: number; // Minimum possible value for a dice roll or undefined if not applicable

  constructor(description: string, value: number, rollString: string) {
    this.description = description;
    this.value = value;

    if (DiceRoll.rollPattern.test(rollString)) {
      let {rolls, sides, modifier} = DiceRoll.splitRollString(rollString);
      this.maxPossible = rolls * sides + modifier;
      this.minPossible = rolls + modifier;
    }
  }

  getDescription() {
    return this.description;
  }

  getValue() {
    return this.value;
  }

  getMaxPossible() {
    return this.maxPossible;
  }

  getMinPossible() {
    return this.minPossible;
  }

}
