
export class Player {

  private _id: String;
  public name: String;
  public notes: String;

  // These are just raw JSON used to populate our services. Use the values in the services instead of these.
  public actions: any;
  public playerStats: any;

  public constructor(id: String, name: String) {
    this._id = id;
    this.name = name;
    this.notes = "";
  }

  public getId() {
    return this._id;
  }

  public setName(name: String) {
    this.name = name;
  }

  public setNotes(notes: String) {
    this.notes = notes;
  }

}
