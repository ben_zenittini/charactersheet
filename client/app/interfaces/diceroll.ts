import {Subaction} from './subaction';
import {RollResult} from './rollresult';

export class DiceRoll {

  public playerName: String;
  public actionName: String;
  public subActions: Subaction[];
  public rollTotal: number;
  public rollResults: Array<RollResult>;

  // Matches a string formatted like d20 or 1d6 or 4d8+5 or 2d17-3
  public static rollPattern = /^([1-9][0-9]*)?d([1-9][0-9]*)(?:([\-\+])([0-9]+))?$/i;

  constructor(playerName: String, actionName: String, subActions: Subaction[]) {
    this.playerName = playerName;
    this.actionName = actionName;
    this.subActions = subActions;
  }

  // Splits a dice-roll string into {rolls, sides, modifier} (e.g. 3d8-4 becomes {3, 8, -4})
  public static splitRollString(value: string) {
    let groups = value.match(DiceRoll.rollPattern);
    if (groups !== null) {
      let rolls: number = 1, sides: number, modifier: number = 0;

      if (groups[1] !== undefined) {
        rolls = parseInt(groups[1]);
      }

      sides = parseInt(groups[2]);

      if (groups[4] !== undefined) {
        modifier = parseInt(groups[4]);
      }

      if (groups[3] !== undefined && groups[3] === '-') {
        modifier *= -1;
      }

      return {rolls, sides, modifier};
    }
    return null;
  }

  // Returns the number obtained by rolling the provided string (or 0 if not a valid dice-roll string)
  public static rollThisString(value: string) {
    let rollObject = DiceRoll.splitRollString(value);
    if (rollObject != null) {
      let result: number = 0;

      for (let i = 0; i < rollObject.rolls; i++) {
        result += Math.floor(Math.random() * rollObject.sides) + 1;
      }
      result += rollObject.modifier;

      return result;
    }
    return 0;
  }

}
