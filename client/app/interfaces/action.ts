
import {Subaction} from './subaction';
import {DiceRoll} from './diceroll';

export class Action {
  static id_counter: number = 0;

  public id: number;
  public name: String;
  public subactions: Subaction[];
  public notes: String;

  public constructor(name: String, subactions: Subaction[], notes: String) {
    this.id = Action.id_counter;
    Action.id_counter++;

    this.name = name;
    this.subactions = subactions;
    this.notes = notes;
  }

  public getName() {
    return this.name;
  }

  public getSubactions() {
    return this.subactions;
  }

  public deleteSubaction(subaction) {
    var index = this.subactions.indexOf(subaction);
    if (index > -1) {
        this.subactions.splice(index, 1);
    }
  }

  public createSubaction() {
    this.subactions.push(new Subaction(-1, "", "0", "", true));
  }

  public addSubaction(subaction: Subaction) {
    this.subactions.push(subaction);
  }
}
