import {SubPlayerStat} from './sub-player-stat';

export class PlayerStat {
  static id_counter: number = 0;

  public id: number;
  public name: String;
  public subPlayerStats: SubPlayerStat[];
  public notes: String;

  public constructor(name: String, subPlayerStats: SubPlayerStat[], notes: String) {
    this.id = PlayerStat.id_counter;
    PlayerStat.id_counter++;

    this.name = name;
    this.subPlayerStats = subPlayerStats;
    this.notes = notes;
  }

  public getName() {
    return this.name;
  }

  public getSubPlayerStats() {
    return this.subPlayerStats;
  }

  public getVisibleSubPlayerStats() {
    var retVals = [];
    for (var entry of this.subPlayerStats) {
      if (entry.showInHeading) {
        retVals.push(entry.key + ": " + entry.value);
      }
    }
    if (retVals.length == 0) return "";
    return retVals.join(" , ")
  }

  public deleteSubPlayerStat(subPlayerStat: SubPlayerStat) {
    var index = this.subPlayerStats.indexOf(subPlayerStat);
    if (index > -1) {
        this.subPlayerStats.splice(index, 1);
    }
  }

  public createSubPlayerStat() {
    this.subPlayerStats.push(new SubPlayerStat(-1, "", "", false));
  }

  public addSubPlayerStat(subPlayerStat: SubPlayerStat) {
    this.subPlayerStats.push(subPlayerStat);
  }
}
