import {Action} from './action';
import {Subaction} from './subaction';

export class ActionCategory {

  public categoryName: String;
  public actions: Action[];

  public constructor(categoryName: String) {
    this.categoryName = categoryName;
    this.actions = [];
  }

  public getName() {
    return this.categoryName;
  }

  public getActions() {
    return this.actions;
  }

  public addAction(action: Action) {
    this.actions.push(action);
  }

  public deleteAction(action: Action) {
    var index = this.actions.indexOf(action);
    if (index > -1) {
      this.actions.splice(index, 1);
    }
  }

}
