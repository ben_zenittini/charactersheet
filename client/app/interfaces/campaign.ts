
import {Player} from './player';

export class Campaign {

  private _id: String;

  public name: String;
  public password: String;
  public players: Player[];
  public activePlayer: Player;

  public constructor(id: String, name: String, password: String, players: Player[]) {
    this._id = id;
    this.setActivePlayer(null);
    this.setName(name);
    this.setPassword(password);
    this.setPlayers(players);
  }

  public getId() {
    return this._id;
  }

  public setName(name: String) {
    this.name = name;
  }

  public setPassword(password: String) {
    this.password = password;
  }

  public getName() {
    return this.name;
  }

  public addPlayer(player: Player) {
    this.players.push(player);
  }

  public setPlayers(players: Player[]) {
    this.players = players;
    this.setActivePlayer(null);
  }

  public getPlayers() {
    return this.players;
  }

  public setActivePlayer(player: Player) {
    this.activePlayer = player;
  }

  public getActivePlayer() {
    return this.activePlayer;
  }

}
