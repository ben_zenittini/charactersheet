export class SubPlayerStat {

  public uuid: number;
  public showInHeading: boolean;
  public key: String;
  public value: String;

  public constructor(uuid: number, key: String, value: String, showInHeading: boolean) {
    if (uuid === -1) {
      this.uuid = Math.floor(Math.random()*1000000000000);
    } else {
      this.uuid = uuid;
    }
    this.showInHeading = showInHeading;
    this.key = key;
    this.value = value;
  }

  setKey(key: string) {
    this.key = key;
  }

  setValue(value: string) {
    this.value = value;
  }
}

