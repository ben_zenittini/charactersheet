import {PlayerStat} from './player-stat';
import {SubPlayerStat} from './sub-player-stat';

export class PlayerStatCategory {

  public categoryName: String;
  public playerStats: PlayerStat[];

  public constructor(categoryName: String) {
    this.categoryName = categoryName;
    this.playerStats = [];
  }

  public getName() {
    return this.categoryName;
  }

  public getPlayerStats() {
    return this.playerStats;
  }

  public addPlayerStat(playerStat: PlayerStat) {
    this.playerStats.push(playerStat);
  }

  public deletePlayerStat(playerStat: PlayerStat) {
    var index = this.playerStats.indexOf(playerStat);
    if (index > -1) {
      this.playerStats.splice(index, 1);
    }
  }

}
