
export class Subaction {

  public uuid: number;
  public name: String;
  public value: String;
  public description: String;
  public isIncluded: boolean;

  public constructor(uuid: number, name: String, value: String, description: String, isIncluded: boolean) {
    if (uuid === -1) {
      this.uuid = Math.floor(Math.random()*1000000000000);
    } else {
      this.uuid = uuid;
    }
    this.name = name;
    this.value = value;
    this.description = description;
    this.isIncluded = isIncluded;
  }

  public setLinkTarget(target: number) {
    this.value="${" + target + "}";
  }

  public getName() {
    return this.name;
  }

  public getValue() {
    return this.value;
  }

  public getDescription() {
    return this.description;
  }

  public getIsIncluded() {
    return this.isIncluded;
  }

}

