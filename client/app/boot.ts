
import {bootstrap} from 'angular2/platform/browser';
import {enableProdMode} from 'angular2/core';

// services
import {ActionService} from './services/action/action.service';
import {FilterService} from './services/filter/filter.service';
import {DiceRollService} from './services/dice-roll/dice-roll.service';
import {PlayerStatService} from './services/player-stat/player-stat.service';
import {CampaignService} from './services/campaign/campaign.service';
import {NetworkingService} from './services/networking/networking.service';
import {NetworkInputService} from './services/networking/network-input.service';
import {NetworkOutputService} from './services/networking/network-output.service';
import {PersistenceService} from './services/persistence/persistence.service';
import {StatlinkService} from './services/statlink/statlink.service';

// Components
import {AppComponent} from './components/app/app.component';

// enableProdMode();

bootstrap(AppComponent, [
  NetworkingService,
  NetworkInputService,
  NetworkOutputService,
  ActionService,
  FilterService,
  DiceRollService,
  PlayerStatService,
  PersistenceService,
  StatlinkService,
  CampaignService]);
