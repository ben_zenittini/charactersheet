import {Component} from 'angular2/core';

import {PlayerStat} from '../../interfaces/player-stat';
import {PlayerStatService} from '../../services/player-stat/player-stat.service';
import {StatlinkService} from '../../services/statlink/statlink.service';
import {PersistenceService} from '../../services/persistence/persistence.service';

@Component({
  selector: 'our-player-stat',
  templateUrl: './app/components/player-stat/player-stat.html',
  inputs: ['playerStat', 'category']
})
export class PlayerStatComponent {

  public playerStat: PlayerStat;
  public category: String;

  public isCollapsed: boolean = true;
  public isInEditMode: boolean = false;

  constructor(
    private _playerStatService: PlayerStatService,
    private _persistenceService: PersistenceService,
    private _statlinkService: StatlinkService) {}

  public getStatlinkService() {
    return this._statlinkService;
  }

  public getPersistenceService() {
    return this._persistenceService;
  }

  public deleteItem() {
    this._playerStatService.deletePlayerStat(this.category, this.playerStat);
  }

  public enterEditMode() {
    this.isInEditMode = true;
    // Focus the edit-name text box
    var that = this;
    setTimeout(function(){
      document.getElementById("player-item-text-" + that.playerStat.id).focus()
    }, 0);
  }

  public exitEditMode() {
    this.isInEditMode = false;
  }

  public inEditMode() {
    return this.isInEditMode;
  }
}
