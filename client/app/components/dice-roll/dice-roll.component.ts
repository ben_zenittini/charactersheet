import {Component, ElementRef} from 'angular2/core';
import {DiceRoll} from '../../interfaces/diceroll';

@Component({
  selector: 'dice-roll',
  inputs: ['roll'],
  templateUrl: './app/components/dice-roll/dice-roll.html',
  providers: [ElementRef]
})
export class DiceRollComponent {
  public roll: DiceRoll;

  constructor(private _elementRef: ElementRef) {}

  public getOpacity() {
    // This fades the dice rolls as they move further down the screen.
    var top = this._elementRef.nativeElement.getBoundingClientRect().top;
    return (window.innerHeight - top) / window.innerHeight;
  }
}
