import {Component} from 'angular2/core';

import {FilterService} from '../../services/filter/filter.service';
import {ActionService} from '../../services/action/action.service';
import {CampaignService} from '../../services/campaign/campaign.service';
import {NetworkOutputService} from '../../services/networking/network-output.service';
import {PersistenceService} from '../../services/persistence/persistence.service';

@Component({
  selector: 'navigation',
  templateUrl: './app/components/navigation/navigation.html'
})
export class NavigationComponent {

  constructor(
    private _filterService: FilterService,
    private _actionService: ActionService,
    private _campaignService: CampaignService,
    private _networkOutputService: NetworkOutputService,
    private _persistenceService: PersistenceService) {}

  public getFilterService() {
    return this._filterService;
  }

  public getActionService() {
    return this._actionService;
  }

  public getNetworkOutputService() {
    return this._networkOutputService;
  }

  public getCampaignService() {
    return this._campaignService;
  }

  public getPersistenceService() {
    return this._persistenceService;
  }

  public getActivePlayer() {
    var activeCampaign = this._campaignService.getActiveCampaign();
    if (activeCampaign === null || activeCampaign.getActivePlayer() === null) {
      return null;
    } else {
      return activeCampaign.getActivePlayer();
    }
  }

  public editNotes() {
    // Focus the textarea
    setTimeout(function(){
      document.getElementById("our-notes-textarea").focus()
    }, 0);
  }
}
