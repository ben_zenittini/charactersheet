import {Component} from 'angular2/core';

import {Action} from '../../interfaces/action';
import {ActionCategory} from '../../interfaces/action-category';
import {ActionComponent} from '../action/action.component';
import {FilterService} from '../../services/filter/filter.service';
import {PersistenceService} from '../../services/persistence/persistence.service';

@Component({
  selector: 'our-action-category',
  templateUrl: './app/components/action-category/action-category.html',
  inputs: ['actionCategory'],
  directives: [ActionComponent]
})
export class ActionCategoryComponent {

  public actionCategory: ActionCategory;

  public filteredActions: any[];

  constructor(private _persistenceService: PersistenceService, private _filterService: FilterService) {
    this.filteredActions = [];
  }

  public getPersistenceService() {
    return this._persistenceService;
  }

  public addAction() {
    this.actionCategory.addAction(new Action("", [], ""));
  }

  public getActions() {
    this.filteredActions.length = 0;
    var temp = this.actionCategory.getActions().filter(action => action.name.toLowerCase().indexOf(this._filterService.getFilterText().toLowerCase()) > -1);
    Array.prototype.push.apply(this.filteredActions, temp);
    return this.filteredActions;
  }

}
