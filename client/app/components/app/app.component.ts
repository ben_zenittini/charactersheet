import {Component} from 'angular2/core';

import {DiceRollService} from '../../services/dice-roll/dice-roll.service';
import {NetworkingService} from '../../services/networking/networking.service';
import {NetworkInputService} from '../../services/networking/network-input.service';
import {NetworkOutputService} from '../../services/networking/network-output.service';
import {ActionService} from '../../services/action/action.service';
import {PlayerStatService} from '../../services/player-stat/player-stat.service';
import {CampaignService} from '../../services/campaign/campaign.service';
import {PersistenceService} from '../../services/persistence/persistence.service';

import {NavigationComponent} from '../navigation/navigation.component';
import {DiceRollComponent} from '../dice-roll/dice-roll.component';
import {ActionCategoryComponent} from '../action-category/action-category.component';
import {PlayerStatCategoryComponent} from '../player-stat-category/player-stat-category.component';

@Component({
  selector: 'my-app',
  templateUrl: './app/components/app/app.html',
  directives: [NavigationComponent, DiceRollComponent, ActionCategoryComponent, PlayerStatCategoryComponent]
})
export class AppComponent {

  constructor(
    private _diceRollService: DiceRollService,
    private _networkingService: NetworkingService,
    private _networkOutputService: NetworkOutputService,
    private _networkInputService: NetworkInputService,
    private _actionService: ActionService,
    private _playerStatService: PlayerStatService,
    private _persistenceService: PersistenceService,
    private _campaignService: CampaignService) {

    // If the user presses "f" and doesn't have a text area or input box selected, then select the filter
    document.addEventListener("keyup", (evt) => {
      if(document.activeElement.tagName.toLowerCase() !== 'input' && document.activeElement.tagName.toLowerCase() !== 'textarea' && evt.keyCode === 70) {
        document.getElementById("ourPageFilter").focus();
      }
    });
  }

  public getPersistenceService() {
    return this._persistenceService;
  }

  public getCampaignService() {
    return this._campaignService;
  }

  public chooseCampaign(camp) {
    var enteredPassword = prompt("Enter the password s'il vous plait:");
    if (enteredPassword !== camp.password) {
      alert("Wrong password dumb dumb.");
      return;
    }
    this._networkOutputService.sendPlayerRequest(camp.getId());
  }

  public createCampaign() {
    var campName = prompt("Enter the campaigny-thing's name:");
    if (campName === null) {
      return;
    }
    var campPass = prompt("Enter the campaigny-thing's password:");
    if (campPass === null) {
      return;
    }
    this._networkOutputService.sendCreateCampaign(campName, campPass);
  }

  public createCharacter() {
    var charName = prompt("Enter your new dude-person's name:");
    if (charName === null) {
      return;
    }
    this._networkOutputService.sendCreateCharacter(charName);
  }

  public getActionCategories() {
    return this._actionService.getActionCategories();
  }

  public getPlayerStatCategories() {
    return this._playerStatService.getPlayerStatCategories();
  }

  public getRolls() {
    return this._diceRollService.getRolls();
  }

  public isCharacterChosen() {
    var activeCampaign = this._campaignService.getActiveCampaign();
    if (activeCampaign === null || activeCampaign.getActivePlayer() === null) {
      return false;
    }
    return true;
  }

  public addActionCategory() {
    var categoryName = prompt("Enter the category name pleases:");
    this._actionService.addCategory(categoryName);
  }

  public addPlayerStatCategory() {
    var categoryName = prompt("Enter the category name pleases:");
    this._playerStatService.addCategory(categoryName);
  }

}
