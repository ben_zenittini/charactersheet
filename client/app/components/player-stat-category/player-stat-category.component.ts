import {Component} from 'angular2/core';

import {PlayerStat} from '../../interfaces/player-stat';
import {PlayerStatCategory} from '../../interfaces/player-stat-category';
import {PlayerStatComponent} from '../player-stat/player-stat.component';
import {PersistenceService} from '../../services/persistence/persistence.service';

@Component({
  selector: 'our-player-stat-category',
  templateUrl: './app/components/player-stat-category/player-stat-category.html',
  inputs: ['playerStatCategory'],
  directives: [PlayerStatComponent]
})
export class PlayerStatCategoryComponent {

  public playerStatCategory: PlayerStatCategory;

  constructor(private _persistenceService: PersistenceService) {}

  public getPersistenceService() {
    return this._persistenceService;
  }

  public addPlayerStat() {
    this.playerStatCategory.addPlayerStat(new PlayerStat("", [], ""));
  }

}
