import {Component} from 'angular2/core';

import {NetworkOutputService} from '../../services/networking/network-output.service';
import {ActionService} from '../../services/action/action.service';
import {DiceRollService} from '../../services/dice-roll/dice-roll.service';
import {CampaignService} from '../../services/campaign/campaign.service';
import {StatlinkService} from '../../services/statlink/statlink.service';
import {PersistenceService} from '../../services/persistence/persistence.service';

import {Action} from '../../interfaces/action';

@Component({
  selector: 'our-action',
  templateUrl: './app/components/action/action.html',
  inputs: ['action', 'category'],
})
export class ActionComponent {

  public action: Action;
  public category: String;

  public isCollapsed: boolean = true;
  public isInEditMode: boolean = false;

  constructor(
    private _networkOutputService: NetworkOutputService,
    private _actionService: ActionService,
    private _persistenceService: PersistenceService,
    private _diceRollService: DiceRollService,
    private _statlinkService: StatlinkService,
    private _campaignService: CampaignService) {}

  public roll() {
    var diceRoll = this._diceRollService.roll(this._campaignService.getActiveCampaign().getActivePlayer().name, this.action);
    this._networkOutputService.sendDiceRoll(diceRoll);
  }

  public getStatlinkService() {
    return this._statlinkService;
  }

  public getPersistenceService() {
    return this._persistenceService;
  }

  public deleteAction() {
    this._actionService.deleteAction(this.category, this.action);
  }

  public enterEditMode() {
    this.isInEditMode = true;
    // Focus the edit-name text box
    var that = this;
    setTimeout(function(){
      document.getElementById("stat-edit-text-" + that.action.id).focus()
    }, 0);
  }

  public exitEditMode() {
    this.isInEditMode = false;
  }

  public inEditMode() {
    return this.isInEditMode;
  }

  public getIncludedStats() {
    var retVals = [];
    for (var entry of this.action.subactions) {
      if (entry.getIsIncluded()) {
        retVals.push(entry.getName() + ": " + this._statlinkService.resolve(entry.getValue()));
      }
    }
    if (retVals.length == 0) return "";
    return retVals.join(" , ")
  }

  public getExcludedStats() {
    var retVals = [];
    for (var entry of this.action.subactions) {
      if (!entry.getIsIncluded()) {
        retVals.push(entry.getName() + ": " + this._statlinkService.resolve(entry.getValue()));
      }
    }
    if (retVals.length == 0) return "";
    var separator = "";
    if (this.getIncludedStats() !== "") {
      separator = " | "
    }
    return separator + retVals.join(" , ")
  }

  // Returns the sum of all the numeric modifiers (excluding dice rolls)
  public getValue() {
    let sum = 0;
    for (let entry of this.action.subactions) {
      if (entry.getIsIncluded()) {
        let value = this._statlinkService.resolve(entry.getValue()).trim();
        // Check if the value coerced to a number is not not a number (thank you, JS); empty string coerces to 0
        // TODO Handle invalid input in a better way
        if (value !== "" && !isNaN(+value)) {
          sum += parseFloat(value);
        }
      }
    }
    return sum;
  }

}
