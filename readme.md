
# How to make it go

### Typescript compiler

From ./client/
```
tsc -w
```

From ./server/
```
tsc -w
```

### Live Webserver

From ./server/
```
node server.js
```

### Sass watcher

From ./client/
```
sass --watch sass/app.scss:css/app.css
```

### Mongo!

To launch mongodb:
```
mongod --config /usr/local/etc/mongod.conf
```

To run mongocli:
```
mongo
```

Some useful mongocli commands:
```
use charactersheet
show collections
db.campaigns.find()
db.campaigns.drop()
```

